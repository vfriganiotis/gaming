import Guns from "./guns.js"
let rocketImg = document.getElementById("rocket");

export default class Paddle {

     constructor(game){
         this.width = 50;
         this.height = 110;
         this.positionX = game.gameWidth/2 - this.width/2;
         this.positionY = game.gameHeight - this.height  - 70;
         this.speed = 0;
         this.maxSpeed = 7;
         this.gameWidth = game.gameWidth;
         this.gameHeight = game.gameHeight;
         this.destroy = false;
         this.shield = false;
         this.imageWidth = 23;
         this.guns = new Guns(this);
     }

     draw(ctx,x,y) {
         ctx.fillStyle = 'red';
         // ctx.fillRect(this.positionX, this.positionY, this.width, this.height)
         ctx.drawImage(rocketImg, x - this.imageWidth, this.positionY , this.width ,this.height );
         this.guns.draw(ctx);
         if(this.shield){
             ctx.beginPath();
             ctx.arc(x, this.positionY + 55, 70, 0, 2 * Math.PI);
             ctx.strokeStyle = '#fff';
             ctx.stroke();
         }
     }

     fire(){
         this.guns.fire();
     }

     moveLeft(){
         this.speed = -this.maxSpeed;
     }

     moveRight(){
         this.speed = this.maxSpeed;
     }

     stop(){
         this.speed = 0;
     }

     update(x,y) {
        // this.positionX += this.speed;
        // if (this.positionX < 0) this.positionX = 0;
        // if (this.positionX + this.width > this.gameWidth)
        //     this.positionX = this.gameWidth - this.width;
         this.positionX = x;
         // this.positionY = y;
     }

}