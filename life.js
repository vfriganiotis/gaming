export default class Life {

    constructor(life){
        this.life = life
        this.initLife = life
    }

    collision(){
        this.life -= 10
    }

    reset(){
        this.life = this.initLife
    }

    draw(ctx) {
        ctx.strokeRect(20, 20, 154, 10);
        ctx.fillStyle = "#FFFFFF";
        ctx.fillRect(22, 22, this.life, 6);
    }

}


