import Bullets  from "./bullets.js"

export default class Guns {

    constructor(paddle){
        this.paddle = paddle
        this.bulletArr = []
    }

    fire(){
        this.bulletArr.push(new Bullets(this.paddle.positionX,this.paddle.positionY))
    }

    remove(item){
        this.bulletArr.splice(item,1)
        console.log(this.bulletArr)
    }

    draw(ctx){
        if(this.bulletArr){
            this.bulletArr.map(function(item){
                // item.y--
                item.y -= 5;
                ctx.fillRect(item.x , item.y , item.width, item.height)
            })
        }
    }

}