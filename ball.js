export default class Ball {

    constructor (positionX,positionY,width,game){
        this.positionX = positionX;
        this.positionY = positionY;
        this.size = width;
        this.speed = {
            x : 15,
            y : 5
        }
        this.gameWidth = game.gameWidth;
        this.gameHeight = game.gameHeight;
        this.paddle =  game.paddle;
    }

    draw(ctx){
        ctx.beginPath();
        ctx.arc(this.positionX, this.positionY, this.size, 0, 2 * Math.PI);
        ctx.fillStyle = 'green';
        ctx.fill();
        ctx.stroke();
    }

    update(deltaTime){
        // ctx.beginPath();
        if( isNaN(deltaTime) ){
            deltaTime = 0;
        }

        this.positionX += this.speed.x;
        this.positionY += this.speed.y;

        if( this.positionX >= this.gameWidth - this.size || this.positionX <= this.size ){

            this.speed.x = -this.speed.x;
            return
        }

        if( this.positionY > this.gameHeight - this.size || this.positionY <= this.size ){

            this.speed.y = -this.speed.y;
            return
        }

        let bottomOfBall = this.positionY + this.size;
        let topOfBall = this.positionY;

        let topOfObject = this.paddle.positionY;
        let leftSideOfObject = this.paddle.positionX;
        let rightSideOfObject = this.paddle.positionX + this.paddle.width;
        let bottomOfObject = this.paddle.positionY + this.paddle.height;

        if (
            bottomOfBall >= topOfObject &&
            topOfBall <= bottomOfObject &&
            this.positionX + this.size >= leftSideOfObject &&
            this.positionX + this.size <= rightSideOfObject
        ){

           this.speed.y = -this.speed.y;
           this.positionY = this.paddle.positionY - this.size;

           return

       }

    }

}