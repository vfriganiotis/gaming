function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

export default class Circles {

    constructor(circles,initBalls){
        this.circles = circles;
        this.init = initBalls;
    }

    reset(){
        this.circles = []
        this.init()
    }

    drawCircle(x,y){
        let dir = 'left';
        if(Math.round(Math.random()) === 1){
            dir = 'right';
        }
        this.circles.push({
            size:1,
            speed: 0,
            positionY: y,
            positionX: x,
            direction:  dir,
            destroy: false,
            remove: false,
            paddleCollision: false,
            velocity: getRandomArbitrary(20,30),
            friction: getRandomArbitrary(1,3),
            timeOut: 1,
            col: "#ffffff"
        });
    }

}
