import Game from './game.js'

let canvas = document.getElementById('GAME')
let ctx = canvas.getContext('2d')

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function randomIntFromInterval(min, max) {
    return Math.floor(min + Math.random()*(max + 1 - min))
}

function wrapText(context, TEXT, x, y, maxWidth, lineHeight) {

    let words = TEXT.split(' ');
    let line = '';

    for(let n = 0; n < words.length; n++) {
        let testLine = line + words[n] + ' ';
        let metrics = context.measureText(testLine);
        let testWidth = metrics.width;
        if (testWidth >= maxWidth && n >= 0) {
            context.textBaseline = 'middle';
            context.textAlign = "center";
            context.fillText(line, x + 250, y);
            line = words[n] + ' ';
            y += lineHeight;
        }
        else {
            line = testLine;
        }
    }

    context.textBaseline = 'middle';
    context.textAlign = "center";
    context.fillText(line , x + 250, y);

}

const canvasWIDTH = window.innerWidth;
const canvasHEIGHT = window.innerHeight;




//Background Strafield
function circlesStar(x,y,radius,vx,vy,style,hue,sat){
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.speedX = vx;
    this.speedY = vy;
    this.velocityX = vx
    this.velocityY = vy
    this.style = style;
    this.friction =  1;
    this.accx = 0;
    this.accy = 0;
    this.hue = hue;
    this.sat = sat;
}

circlesStar.prototype.render = function (ctx) {

    this.x += this.speedX;
    this.y +=  this.speedY;

    if(this.y > window.innerHeight){
        this.y = 0
    }
    if( this.y < 0){
        this.y = window.innerHeight
    }

    if(this.x > window.innerWidth){
        this.x = 0
    }
    if( this.x < 0){
        this.x = window.innerWidth
    }

    ctx.beginPath()
    ctx.arc(this.x,this.y,this.radius, 0 , 2 * Math.PI)
    ctx.fillStyle =  "hsl(" + this.hue + ", " + this.sat + "%, 88%)";
   // ctx.fillStyle = "#fff";
    ctx.fill();
    ctx.closePath()

}

let circlesArr = []
let colorrange = [0,60,240];

for( let i = 0; i < 350; i++ ){
    let xs  = (Math.random() * window.innerWidth) + 1
    let ys  =  (Math.random() * window.innerHeight) + 1
    let radiuss = Math.random() * 1.2;
    let vxs = randomIntFromInterval(-0.1,0.1)/6;
    let vys = randomIntFromInterval(-0.1,0.1)/6;
    let styles = randomIntFromInterval(0,5)
    let hue = colorrange[randomIntFromInterval(0,colorrange.length - 1)]
    let sat = randomIntFromInterval(50,100);
    circlesArr.push(new circlesStar(xs, ys,radiuss,vxs,vys,styles,hue,sat))
}

//ctx,gameWidth,gameHeight,paddle,ball,controller

let bgCanvas = document.createElement('canvas');
bgCanvas.width = window.innerWidth;
bgCanvas.height = 350;
let bgCtx = bgCanvas.getContext('2d');

let maxWidth = 500;
let lineHeight = 27;
let x = (canvasWIDTH - maxWidth) / 2;
let y = 60;
let textR = 'NIKOS It is a long established fact that a reader will be distracted by th NIKOS It is a long established fact that a reader will be distracted by th NIKOS It is a long established fact that a reader will be distracted by th'
bgCtx.fillStyle = "#fff";
bgCtx.fillRect(0, 0, bgCanvas.width, bgCanvas.height);
bgCtx.fillStyle = '#000';
bgCtx.font = '22px Open sans';

wrapText(bgCtx, textR, x, y, maxWidth, lineHeight);

//Circles Array
let circles = [];
let pixel,imageData,width,height

let densess = 1;

imageData = bgCtx.getImageData(0,0,bgCanvas.width,bgCanvas.height)

function init() {
    let index
    let r,g,b,a
    for (height = 0; height < bgCanvas.height; height += densess) {
        for (width = 0; width < bgCanvas.width; width += densess) {
            //We get the PIXEL DATA

            //  pixel = imageData.data[(width + height * bgCanvas.width) * 4 - 1];
            // //And Also Check for the Pixel Value and Render a Circle of true on the init function
            // if ( pixel === 0 ) {
            //     drawCircle(width, height); ///< we draw on each pixel
            // }
            //find current pixel

            index = (width + height * bgCanvas.width) * 4;
            //separate into color values
            r = imageData.data[index];
            g = imageData.data[index + 1];
            b = imageData.data[index + 2];
            a = imageData.data[index + 3];

            if( r === 0 && g === 0 && b === 0){
                drawCircle(width, height); ///< we draw on each pixel
            }
        }
    }
}

function drawCircle(x, y) {

    let dir = 'left';
    if(Math.round(Math.random()) === 1){
        dir = 'right';
    }
    circles.push({
        size:1,
        speed: 0,
        positionY: y,
        positionX: x,
        direction:  dir,
        destroy: false,
        remove: false,
        paddleCollision: false,
        velocity: getRandomArbitrary(20,30),
        friction: getRandomArbitrary(1,3),
        timeOut: 1,
        col: "#ffffff"
    });
}

init();

function ReInit() {
    let index
    let r,g,b,a
    for (height = 0; height < bgCanvas.height; height += densess) {
        for (width = 0; width < bgCanvas.width; width += densess) {
            index = (width + height * bgCanvas.width) * 4;
            r = imageData.data[index];
            g = imageData.data[index + 1];
            b = imageData.data[index + 2];
            a = imageData.data[index + 3];
            if( r === 0 && g === 0 && b === 0){
                this.drawCircle(width, height); ///< we draw on each pixel
            }
        }
    }
}
//Get Positon Bg Canvas
let bgCanvasPosition  = document.getElementById('text-real')

let bgCanvasRect = {
    left: 100,
    right: 900,
    top: 50,
    bottom: 200
}

let mouseX,mouseY;

canvas.onmousemove = function(e){
    // console.log(e)
    mouseX = e.offsetX
    mouseY = e.offsetY
};

canvas.setAttribute('width',window.innerWidth)
canvas.setAttribute('height',window.innerHeight)

let game = new Game(ctx,canvasWIDTH,canvasHEIGHT,circles,bgCanvasRect,ReInit)
let lastime = 0;


let rocketImg = document.getElementById("planet");

let brick = document.getElementById("brick");
let angle = Math.PI
let degrees = 30
function paint(timestamp){

    let time = timestamp - lastime;

    lastime = timestamp;


    game.start(mouseX,mouseY)

    for( let i = 0; i < circlesArr.length; i++){
        circlesArr[i].render(ctx)
    }
    // degrees++
    // ctx.setTransform(1,0,0,1,0,0);
    ctx.save(); // save current state
    //ctx.rotate(degrees*Math.PI/180); // rotate
 //   ctx.clearRect(0, 0, canvas.width, canvas.height); //clear the canvas
    ctx.translate(300, 300); //let's translate
    ctx.rotate(Math.PI / 180 * (degrees += 0.5)); //increment the angle and rotate the image
    ctx.drawImage(rocketImg, -120 / 2, -80 / 2, 80, 80); //draw the image ;)
    ctx.restore(); //restore the state of canvas
   // ctx.drawImage(brick,300,300,80,80); // draws a chain link or dagger
   //  ctx.translate(-(canvas.width/2), -(canvas.height/2)); //let's translate
   //  ctx.restore(); // restore original states (no rotation etc)

    //update
    requestAnimationFrame(paint)

}

paint()
