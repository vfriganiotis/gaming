export default class Brick {

    constructor(img,positionX,positionY){
        this.x = 30;
        this.y = 30;
        this.img = img;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    draw(ctx){


        ctx.drawImage(this.img, this.positionX, this.positionY , 120 ,50 );

    }

    remove(ctx){

        ctx.drawImage(this.img, this.positionX , this.positionY , 120 ,50 );

    }

}