export default class Bullets {

    constructor(x,y){
        this.fire = false
        this.loads  = 0
        this.fires  = 0
        this.speed  = 15
        this.width  = 10
        this.height  = 10
        this.x  = x
        this.y  = y
    }

    fire(){
        this.fires++
        this.loads++
        this.fire = false;
        this.speed += this.speed;
    }

}