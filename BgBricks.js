import BrickBG from "./brickBG.js"

export default class BgBricks {

    constructor(bullets){
        this.bullets = bullets;
        this.brickArr = [];
    }

    draw(ctx,bgc){
        if( this.brickArr.length !== 0){
            for (let i = 0; i < this.brickArr.length; i++) {
                this.brickArr[i].draw(ctx)
            }
        }
        DetectCollision(this.bullets,bgc,this.brickArr)
    }

    reset(){
        this.brickArr = []
    }

}

function DetectCollision(bullets,bgc,brickArr){
    bullets.map(function(item,num){
        let topOfGun = item.y;
        let leftOfGun = item.x;
        let rightOfGun = item.x + item.width;
        let bottomOfGun = item.y + item.height;
        if( bgc.bottom > topOfGun  &&  !item.destroy ){
            item.destroy = true
            PushBgNewBrick(brickArr,leftOfGun,bottomOfGun)
        }
    })
}

function PushBgNewBrick(array,x,y){
    return  array.push(new BrickBG(x,y,20,1))
}
