import Paddle from './paddle.js'
import Controller from './controller.js'
import Life from "./life.js"
import Circles from "./circles.js"
import BgBricks from "./BgBricks.js"

let img = document.getElementById("planet");

function getDistance(x1,x2,y1,y2){
    var a = x1 - x2;
    var b = y1 - y2;

    var c = Math.sqrt( a*a + b*b );
    return c
}

export default class Game{

    constructor ( ctx,gameWidth,gameHeight , circles ,bgCanvasPosition ,initBalls ) {
        this.ctx = ctx;
        this.lifescore = 150;
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
        this.audio = document.getElementById("myAudio");
        this.audioExplosion = document.getElementById("myAudioExplosion");
        this.shield = false;
        this.bgCanvasPosition = bgCanvasPosition;
        this.paddle = new Paddle(this);
        this.circles = new Circles(circles,initBalls);
        this.brickBG = new BgBricks(this.paddle.guns.bulletArr)
        this.life = new Life(this.lifescore);
        this.controller = new Controller(this.paddle,this.audio,this.circles,this.brickBG,this.life);
    }

    start(x,y){
        this.ctx.clearRect( 0, 0, this.gameWidth, this.gameHeight);
        this.render()
        this.paddle.update(x,y)
        this.paddle.draw(this.ctx,x,y)
        this.life.draw(this.ctx)
        this.controller.drawShield(this.paddle)
    }

    render(){

        this.brickBG.draw(this.ctx,this.bgCanvasPosition)

        DetectCollissionCirclePaddle(this.circles.circles,this.paddle,this.audioExplosion,this.life)

        if(this.circles.circles){
            for (let i = 0; i < this.circles.circles.length; i++) {
                let circ = this.circles.circles[i];

                //Start Particle Path

                if(circ.speed === 0){
                    // this.ctx.beginPath();
                    // this.ctx.arc(circ.positionX,circ.positionY , circ.size, 0, Math.PI * 2, false);
                    // this.ctx.fillStyle = '#FFF';
                    // this.ctx.fill();
                    // this.ctx.closePath();
                }else{
                    //Update Particle After Collision
                    circ.friction -= 0.05
                    if( circ.direction === 'left' ){
                        circ.positionY = circ.positionY  + (circ.speed++ / circ.velocity ) * -circ.friction
                        circ.positionX = circ.positionX   - circ.velocity / 50
                    }else{
                        circ.positionY = circ.positionY  + (circ.speed++ / circ.velocity) * -circ.friction
                        circ.positionX = circ.positionX + circ.velocity / 50
                    }
                    //Draw Particle
                    this.ctx.beginPath();
                    this.ctx.arc(circ.positionX , circ.positionY , circ.size, 0, Math.PI * 2, false);
                    this.ctx.fillStyle = this.circles.circles[i].col;
                    this.ctx.fill();
                    this.ctx.closePath();
                }

                //Remove Particle if position is bigger than Canvas Height
                if(circ.positionY > this.gameHeight){
                    this.circles.circles.splice(i,1)
                }

                //Remove Particle if has collision with paddle
                if(circ.remove){
                    circ.timeOut -= 0.05
                    if( circ.timeOut < 0){
                        this.circles.circles.splice(i,1)
                    }
                }

            }
        }

        if(this.circles.circles.length === 0){
            console.log('finish')
        }
        if(this.lifescore === 0){
            console.log('lost')
        }
    }

}

function DetectCollissionCirclePaddle(circles,paddle,audio,life){

    for( let i =0; i < circles.length; i++){

        let leftSideOfObject = circles[i].positionX;
        let topOfObject = circles[i].positionY;
        let rightSideOfObject = circles[i].positionX;
        let bottomOfObject = circles[i].positionY + 20;

        // paddle.positionX
        let paddletopOfGun = paddle.positionY;
        let paddleleftOfGun = paddle.positionX - paddle.imageWidth;
        let paddlerightOfGun = paddle.positionX - paddle.imageWidth + paddle.width;
        let paddlebottomOfGun = paddle.positionY + paddle.height;

        if (
            paddlebottomOfGun >= topOfObject &&
            paddletopOfGun <= bottomOfObject &&
            paddlerightOfGun >= leftSideOfObject &&
            paddleleftOfGun <= rightSideOfObject &&
            !circles[i].paddleCollision &&
            !paddle.shield
        ) {
            life.collision()
            circles[i].paddleCollision = true
            circles[i].friction = 0
            circles[i].remove = true
        }

        if(paddle.shield && Math.floor(getDistance( paddleleftOfGun + 20 , leftSideOfObject , paddletopOfGun + 60, bottomOfObject) ) <= 70){
            circles[i].paddleCollision = true
            circles[i].friction = 0.5
            circles[i].remove = true
        }

        paddle.guns.bulletArr.map(function(item,num){

            if( item.y < 0){
                paddle.guns.remove(num)
            }

            let topOfGun = item.y;
            let leftOfGun = item.x;
            let rightOfGun = item.x + item.width;
            let bottomOfGun = item.y + item.height;

            if (
                bottomOfGun >= topOfObject &&
                topOfGun <= bottomOfObject &&
                rightOfGun >= leftSideOfObject &&
                leftOfGun <= rightSideOfObject &&
                !circles[i].destroy
            ) {
                circles[i].speed = 10
                circles[i].destroy = true
                // audio.pause()
                audio.currentTime = 0.8
                audio.play()
            }

        })
    }
}
