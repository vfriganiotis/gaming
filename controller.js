const initShieldOptions = {
    timer : 1,
    hold: false
}

export default class Controller {

   constructor (paddle,audio,circles,bgbricks,life){

       document.addEventListener('keydown', function (event) {

           switch(event.keyCode){
               case 65 :
                   paddle.moveLeft()
                 break;
               case 68 :
                   paddle.moveRight()
                 break;
               case 69 :
                   paddle.fire()
                   break;
               case 81 :
                   paddle.fire()
                   break;
           }

       })

       document.addEventListener('keyup', function (event) {

           paddle.shield = true;

           switch(event.keyCode){
               case 65 :
                   if(paddle.speed < 0)
                       paddle.stop()
                   break;
               case 68 :
                   if(paddle.speed > 0)
                       paddle.stop()
                   break;
           }

       })

       document.addEventListener('mousedown', function (event) {
           initShieldOptions.hold = true
           initShieldOptions.time = 1
           paddle.fire()
           audio.pause()
           audio.currentTime = 0
           audio.play()
       })

       document.addEventListener('mouseup', function (event) {
           initShieldOptions.hold = false
           initShieldOptions.time = 1
         //  initShieldOptions.paddle.shield = false;
       })

       let resetButton = document.getElementById('reset')
       resetButton.addEventListener('click',function(e){
           circles.reset()
           bgbricks.reset()
           life.reset()
       })

   }

   drawShield(paddle) {
       if(initShieldOptions.hold){
           initShieldOptions.time -= 0.05
           if( initShieldOptions.time < 0){
               paddle.shield = true;
           }
       }else{
           paddle.shield = false;
       }
   }

}


